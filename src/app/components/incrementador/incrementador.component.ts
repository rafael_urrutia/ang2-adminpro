import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: []
})
export class IncrementadorComponent implements OnInit {

  @Input('nombre') leyenda: string = 'Leyenda';
  @Input() progreso: number = 50;

  @Output('actualizarValor') cambioValor: EventEmitter<number> = new EventEmitter();

  @ViewChild('txtProgreso') txtProgreso: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  onChanges(newValue: number){

    this.cambioValor.emit(this.progreso);
    if(newValue >= 100){
      this.progreso = 100;
    }else if(newValue <=0){
      this.progreso = 0;
    }else{
      this.progreso = newValue;
    }

    this.txtProgreso.nativeElement.value = this.progreso;
    this.cambioValor.emit(this.progreso);

  }

  cambiarValor(valor){

    if(this.progreso<=0 && valor <0){
      this.progreso = 0;
      return;
    }

    if(this.progreso>=100 && valor >0){
      this.progreso = 100;
      return;
    }

    this.progreso = this.progreso + valor;
    this.cambioValor.emit(this.progreso);

    this.txtProgreso.nativeElement.focus();
  }

}
